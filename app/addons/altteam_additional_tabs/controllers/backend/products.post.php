<?php

use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'add') {
    $product_faker_id = 0;

    $product_data = Tygh::$app['view']->getTemplateVars('product_data');
    $auth = Tygh::$app['session']['auth'];

    $product_tab_params = array(
        'dynamic_object' => array(
            'object_type' => 'products',
            'object_id' => $product_faker_id
        ),
        'product_id' => $product_faker_id
    );

    Registry::set(
        'navigation.tabs.product_tabs',
        [
            'title' => __('product_details_tabs'),
            'href' => 'tabs.manage_in_tab?' . http_build_query($product_tab_params),
            'ajax' => true,
        ]
    );

    $product_data['product_id'] = $product_faker_id;

    $product_faker_data = [
        'category_ids' => !empty($_REQUEST['category_ids']) ? $_REQUEST['category_ids'] : '',
        'product_id' => [$product_faker_id]
    ];

    list($product_features, $features_search) = fn_get_paginated_product_features(
        array('product_id' => $product_faker_id),
        $auth,
        $product_faker_data,
        DESCR_SL
    );

    $unset_tabs = fn_get_unset_tabs();
    Registry::set('navigation.tabs', $unset_tabs);

    Tygh::$app['view']->assign('product_features', $product_features);
    Tygh::$app['view']->assign('features_search', $features_search);
    Tygh::$app['view']->assign('product_data', $product_data);
} elseif ($mode == 'update') {

    $product_data = Tygh::$app['view']->getTemplateVars('product_data');
    $unset_tabs = fn_get_unset_tabs();

    $product_data['category_ids'] = !empty($_REQUEST['category_ids']) ? $_REQUEST['category_ids'] : '';
    list($product_features, $features_search) = fn_get_paginated_product_features(
        array('product_id' => !empty($product_data['product_id']) ? $product_data['product_id'] : ''),
        $auth,
        $product_data,
        DESCR_SL
    );

    Tygh::$app['view']->assign('product_features', $product_features);
    Tygh::$app['view']->assign('features_search', $features_search);
    Registry::set('navigation.tabs', $unset_tabs);
}

function fn_get_unset_tabs()
{
    $unset_tabs = [
        'seo',
        'qty_discounts',
        'addons',
        'shippings',
        'features'
    ];

    $general_tabs = Registry::get('navigation.tabs');

    foreach ($unset_tabs as $un_tab) {
        unset($general_tabs[$un_tab]);
    }
    return $general_tabs;
}
